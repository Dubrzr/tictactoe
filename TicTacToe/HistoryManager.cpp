#include "stdafx.h"
#include "HistoryManager.h"
#include "Board.h"
#include <iostream>
#include <fstream>

using namespace std;

HistoryManager::HistoryManager(int size) : boards_size(size) 
{
	Board * new_board = new Board(size);
	b_stack.push(new_board);
	current_board = b_stack.top();
}

HistoryManager::~HistoryManager()
{
	while (!b_stack.empty())
	{
		b_stack.top()->~Board();
		*b_stack.top() = 0;
		b_stack.pop();
	}
}

// SET - BACK
bool HistoryManager::set(Board::val player, int x, int y)
{
	if (x > 0 && x <= boards_size && y > 0 && y <= boards_size && b_stack.top()->get(x, y) == Board::E)
	{
		Board * new_board = new Board(b_stack.top());
		new_board->set(player, x, y);
		b_stack.push(new_board);
		current_board = b_stack.top();
		return true;
	}
	else
		return false;
}
void HistoryManager::ComeBack(Board::val player, int available_tiles)
{
	if (b_stack.empty())
		return;
	b_stack.top()->~Board();
	b_stack.pop();
	available_tiles++;
	if (b_stack.size() < 2)
	{
		if (player == Board::O)
			player = Board::X;
		return;
	}
	b_stack.top()->~Board();
	b_stack.pop();
	available_tiles++;
	current_board = b_stack.top();
	return;
}

// SAVE HISTORY TO FILE
void  HistoryManager::saveStepsToFile(string path)
{
	ofstream a_file(path);
	while (!b_stack.empty())
	{
		HistoryManager::writeMatrix(&a_file, b_stack.top());
		b_stack.top()->~Board();
		b_stack.pop();
	}
	a_file.close();
}
void HistoryManager::writeMatrix(ofstream * stream, Board * b)
{
	int line_length = b->size + (b->size - 1) * 2 + 2;
	*stream << "*";
	for (int k = 0; k < line_length; ++k)
		*stream << "-";
	*stream << "*";

	for (int i = 0; i < b->size; ++i)
	{
		*stream << endl << "|";
		for (int j = 0; j < b->size; ++j)
			*stream << b->val_to_string(b->get(i+1, j+1));
		*stream << "|" << endl;
		if (i < b->size - 1)
		{
			*stream << "|";
			for (int k = 0; k < line_length; ++k)
				*stream << " ";
			*stream << "|";
		}
	}
	*stream << "*";
	for (int k = 0; k < line_length; ++k)
		*stream << "-";
	*stream << "*" << endl;
}