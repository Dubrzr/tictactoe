#pragma once
class Board
{
public:

	int size;
	typedef enum{E,	X, O} val;

	Board(int);
	Board(Board *);
	~Board();

	val get(int, int);
	void print();
	char * val_to_string(val);
	bool isWinned(val, int, int, int);
	void set(val, int, int);


private:

	val ** matrix;
	int rec_win(val, int, int, int, int);

	val ** copyBoard(Board *);
};