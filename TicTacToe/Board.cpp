#include "stdafx.h"
#include "Board.h"
#include <iostream>
#include <string>

using namespace std;


// CONSTRUCTORS
Board::Board(int dims)
{
	size = dims;
	matrix = new val*[size];
	for (int i = 0; i < size; ++i)
		matrix[i] = new val[size];

	for (int i = 0; i < size; ++i)
	for (int j = 0; j < size; ++j)
		matrix[i][j] = E;
}
Board::Board(Board * b)
{
	size = b->size; 
	matrix = copyBoard(b);
}

// DESTRUCTOR
Board::~Board()
{
	if (matrix)
	{
		for (int j = 0; j < size; ++j)
			delete[] matrix[j];
		delete[] matrix;
		matrix = 0;
	}
}

// GETTER - SETTER
Board::val Board::get(int x, int y) { return matrix[x-1][y-1]; }
void Board::set(val player, int x, int y) { matrix[x-1][y-1] = player; print(); }


// iostream PRINTER
void Board::print()
{
	system("cls");
	int line_length = size + (size - 1) * 2 + 2;
	cout << "            *";
	for (int k = 0; k < line_length; ++k)
		cout << "-";
	cout << "*";

	for (int i = 0; i < size; ++i)
	{
		cout << endl << "            |";
		for (int j = 0; j < size; ++j)
			cout << val_to_string(matrix[i][j]);
		cout << "|" << endl;
		if (i < size - 1)
		{
			cout << "            |";
			for (int k = 0; k < line_length; ++k)
				cout << " ";
			cout << "|";
		}
	}
	cout << "            *";
	for (int k = 0; k < line_length; ++k)
		cout << "-";
	cout << "*" << endl;
}

// Winner finder
int Board::rec_win(val player, int x, int y, int d_x, int d_y)
{
	if (x < 0 || y < 0 || x >= size || y >= size)
		return 0;
	else if (matrix[x][y] == player)
		return 1 + rec_win(player, x + d_x, y + d_y, d_x, d_y);
	else
		return 0;
}
bool Board::isWinned(val player, int need, int x, int y)
{
	int count = 0;

	// horizontal
	if (rec_win(player, x, y, -1, 0) + rec_win(player, x, y, 1, 0) > need)
		return true;
	// vertical
	if (rec_win(player, x, y, 0, -1) + rec_win(player, x, y, 0, 1) > need)
		return true;
	// diag 1
	if (rec_win(player, x, y, -1, -1) + rec_win(player, x, y, 1, 1) > need)
		return true;
	// diag 2
	if (rec_win(player, x, y, -1, 1) + rec_win(player, x, y, 1, -1) > need)
		return true;

	return false;
}


// TOOLS
char * Board::val_to_string(Board::val token) 
{
	switch (token)
	{
	case X: return " X ";
	case O: return " O ";
	default: return "   ";
	}
}
Board::val ** Board::copyBoard(Board * b)
{
	Board::val ** new_matrix = new val*[b->size];
	for (int i = 0; i < b->size; ++i)
		new_matrix[i] = new val[b->size];

	for (int i = 0; i < b->size; ++i)
	for (int j = 0; j < b->size; ++j)
		new_matrix[i][j] = b->get(i+1, j+1);

	return new_matrix;
}