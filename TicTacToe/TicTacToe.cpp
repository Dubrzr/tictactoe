#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>

#include "Board.h"
#include "HistoryManager.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"

#define ANSI_COLOR_BRIGHT  "\x1b[1m"
#define ANSI_COLOR_RESET   "\x1b[0m"

using namespace std;





int _tmain()
{
	system("mode 73, 40");
	system("COLOR F1");
	system("cls");

	int k = 0;
	int mode = 0;
	int need = 0;
	int pos_x = -1, pos_y = -1;
	int max_need = 0;
	int available_tiles = 0;
	
	while (k < 3 || k > 15)
	{
		cout << "What size do you want? (3->15) ";
		cin >> k;
	}
	available_tiles = k * k;

	HistoryManager * hm = new HistoryManager(k);

	if (k == 4)	max_need = 4;
	else if (k > 4) max_need = 5;

	if (max_need > 3)
	{
		while (need < 3 || need > max_need)
		{
			system("cls");
			cout << "How much should you align to win? (3->" << max_need << ") ";
			cin >> need;
		}
	}
	else
		need = 3;
	while (mode != 1 && mode != 2)
	{
		system("cls");
		cout << "In Which mode do you want to play ? 1 : (1 VS 1) / 2 : (1 VS IA)? (1/2) : ";
		cin >> mode;
	}

	Board::val current_player = Board::O;

	// 1 VS 1
	if (mode == 1)
	{
		while (!hm->current_board->isWinned(current_player, need, pos_x - 1, pos_y - 1) && available_tiles > 0)
		{
			pos_x = -1, pos_y = -1;
			if (k == 1)
			{
				if (current_player == Board::O) current_player = Board::X;
				else current_player = Board::O;
			}
			k = 0;
			while (k != 1 && k != 2)
			{
				hm->current_board->print();
				cout << endl << "Player " << current_player << " : What do you want to do ? (Set = 1 / Back = 2) ";
				cin >> k;
			}

			if (k == 1)
			{
				pos_x = -1, pos_y = -1;
				while (!hm->set(current_player, pos_x, pos_y))
				{
					pos_x = -1, pos_y = -1;
					while (pos_x < 1 || pos_x > hm->boards_size && pos_y < 1 || pos_y > hm->boards_size)
					{
						hm->current_board->print();
						cout << endl << "Player " << current_player << " : Where do you want to set " << endl << " x : 1 -> " << hm->boards_size << " ? ";
						cin >> pos_y;
						cout << endl << " y : 1 -> " << hm->boards_size << " ? ";
						cin >> pos_x;
					}
				}
				available_tiles--;
			}
			else
			{
				hm->ComeBack(current_player, available_tiles);
			}
		}
		hm->current_board->print();
		if (available_tiles < 1)
			cout << "No one wins...";
		else
			cout << "Winner is : " << current_player << ", Congratulations ! :D" << endl;
		getchar();

	}
	else
	{
		int cu_pl = 0;
		while (cu_pl < 1 && cu_pl > 2)
		{
			cout << "Who plays first? (1 = You / 2 = IA)";
			cin >> cu_pl;
		}
		if (cu_pl == 1) // Player
			current_player = Board::O;
		else // IA
			current_player = Board::X;
	}
	k = 0;
	while (k != 1 && k != 2)
	{
		cout << endl << "Do you want to save to results.txt ? (No=1/Yes=2) ";
		cin >> k;
	}
	if (k == 2)
		hm->saveStepsToFile();

	k = 0;
	while (k != 1 && k != 2)
	{
		cout << endl << "Do you want to plain again ? (No=1/Yes=2) ";
		cin >> k;
	}
	delete hm;
	if (k == 2)
		_tmain();
	return 0;
}

