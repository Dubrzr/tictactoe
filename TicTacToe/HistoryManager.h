#pragma once
#include <stack>
#include "Board.h"
#include <string>
using namespace std;

class HistoryManager
{
public:
	HistoryManager(int);
	~HistoryManager();

	int boards_size;
	Board * current_board;

	bool set(Board::val, int, int);

	void ComeBack(Board::val, int);
	void saveStepsToFile(string path = "result.txt");
	void writeMatrix(ofstream *, Board *);

private:
	stack <Board*> b_stack;
};

